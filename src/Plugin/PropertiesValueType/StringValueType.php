<?php

namespace Drupal\properties_field\Plugin\PropertiesValueType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\properties_field\PropertiesValueType\PropertiesValueTypeBase;

/**
 * Provides the string properties value type plugin.
 *
 * @PropertiesValueType(
 *   id = "string",
 *   label = @Translation("String"),
 * )
 */
class StringValueType extends PropertiesValueTypeBase {

  /**
   * {@inheritdoc}
   */
  public function widgetForm(array $element, $value, FormStateInterface $form_state) {
    $element = parent::widgetForm($element, $value, $form_state);
    $element['#maxlength'] = NULL;

    return $element;
  }

}
